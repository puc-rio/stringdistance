local d = require"stringdistance"

-- Canção do exílio - Gonçalves Dias
local str1 = "Minha terra tem palmeiras, Onde canta o Sabiá; As aves, que aqui gorjeiam, Não gorjeiam como lá. Nosso céu tem mais estrelas, Nossas várzeas têm mais flores, Nossos bosques têm mais vida,  Nossa vida mais amores. Em  cismar, sozinho, à noite, Mais prazer eu encontro lá; Minha terra tem palmeiras, Onde canta o Sabiá. Minha terra tem primores, Que tais não encontro eu cá; Em cismar sozinho, à noite Mais prazer eu encontro lá; Minha terra tem palmeiras, Onde canta o Sabiá. Não permita Deus que eu morra, Sem que eu volte para lá; Sem que desfrute os primores Que não encontro por cá; Sem quinda aviste as palmeiras, Onde canta o Sabiá."
-- Exílio da Canção do Exílio - Adriel Gael
local str2 = "Minha terra não tem mais palmeiras, Muito menos sabiá; As aves, que aqui gorjearam, Não gorjeiam mais. Nosso céu não tem mais estrelas, Em nossas várzeas secaram as flores, Nosso bosque não tem mais vidas, Nem nossas vidas mais amores. Quando estou, sozinho, à noite, Não encontro mais prazer eu cá, Minha terra não tem mais palmeira, Muito menos sabiá. Minha terra não tem mais primores, Que tais eu encontrava cá, Quando estou sozinho à noite Não encontro mais prazer eu lá; Minha terra não tem mais palmeiras, Muito menos o sabiá. Permita, oh! Deus que eu morra, Antes de ver tudo acabado, Não desfruto mais os primores, Que encontrava por cá, Não avisto mais as palmeiras, Onde cantava o sabiá."


assert(arg[1], "Error at second argument. Use '-l' to Levenshtein '-d' to Damerau-Levenshtein")

local n = tonumber(arg[2]) or 1000
local alg

if (arg[1] == "-l") then
	alg = "Levenshtein"
	f = d.lev
elseif (arg[1] == "-d") then
	alg = "Damerau-Levenshtein"
	f = d.dam
end

local i = 0
while i < n do
   	local _ = f(str1,str2)
    i = i + 1
end

print(string.format("%s running %d times.", alg, n))
